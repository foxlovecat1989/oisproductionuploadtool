package com.ed.tpi;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class AppConstant {
    private static final String CONFIG_FILE_PATH = "src/main/resources/config/config.txt";
    public static final String TOKEN_SPLIT_VALUE = ": ";
    public static final String TOKEN_SPLIT_KEY = ":";

    private static final Map<String, String> constants = FileTool.readFile(CONFIG_FILE_PATH).stream()
            .collect(Collectors.toMap(
                    line -> line.substring(0, line.indexOf(TOKEN_SPLIT_KEY)),
                    line -> line.substring(line.indexOf(TOKEN_SPLIT_VALUE) + TOKEN_SPLIT_VALUE.length(), line.lastIndexOf(";"))));
    private static final String DEFAULT_WINDOWS_FORWARD_SLASH = "\\";
    private static final String DEFAULT_LINUX_BACKWARD_SLASH = "/";
    private static final String DEFAULT_WINDOWS_CMD = "cmd.exe";
    private static final String DEFAULT_LINUX_SH = "bash";

    public static final String LINUX_BACKWARD_SLASH = getLinuxBackwardSlash();
    public static final String WINDOWS_FORWARD_SLASH = getWindowsForwardSlash();
    public static final String WINDOWS_CMD = getWindowsCmd();
    public static final String LINUX_SH = getLinuxSh();
    public static final String SOURCE_PATH_PREFIX = getSourcePathPrefix();
    public static final String PROJECT_NAME = getProjectName();
    public static final String PROJECT_PATH = getProjectPath();
    public static final String DEST_PATH = getDestPath();
    public static final String SHELL_PATH = getShellPath();
    public static final String BRANCH_NAME = getBranchName();
    public static final String CONFIG_FILE_PATH = getConfigFilePath();

    private AppConstant() {
    }

    public static String getLinuxBackwardSlash() {
        return Optional.ofNullable(constants.get("LINUX_BACKWARD_SLASH")).orElse(DEFAULT_LINUX_BACKWARD_SLASH);
    }

    public static String getWindowsForwardSlash() {
        return Optional.ofNullable(constants.get("WINDOWS_FORWARD_SLASH")).orElse(DEFAULT_WINDOWS_FORWARD_SLASH);
    }

    public static String getWindowsCmd() {
        return Optional.ofNullable(constants.get("WINDOWS_CMD")).orElse(DEFAULT_WINDOWS_CMD);
    }

    public static String getLinuxSh() {
        return Optional.ofNullable(constants.get("LINUX_SH")).orElse(DEFAULT_LINUX_SH);
    }

    public static String getSourcePathPrefix() {
        return Optional.ofNullable(constants.get("SOURCE_PATH_PREFIX")).orElse("");
    }

    public static String getProjectName() {
        return Optional.ofNullable(constants.get("PROJECT_NAME")).orElse("");
    }

    public static String getProjectPath() {
        return Optional.ofNullable(constants.get("PROJECT_PATH")).orElse("");
    }

    public static String getDestPath() {
        return Optional.ofNullable(constants.get("DEST_PATH")).orElse("");
    }

    public static String getShellPath() {
        return Optional.ofNullable(constants.get("SHELL_PATH")).orElse("");
    }

    public static String getBranchName() {
        return Optional.ofNullable(constants.get("DEFAULT_BRANCH_NAME")).orElse("");
    }

    private static String getConfigFilePath() {
        return Optional.ofNullable(constants.get("CONFIG_FILE_PATH")).orElse("");
    }
}
