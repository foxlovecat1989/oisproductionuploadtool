package com.ed.tpi;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;

@Slf4j
public class FileTool {

    public static void copyFile(String sourceFilePath, String destinationFilePath) {
        try {
            log.info("copy: {}", sourceFilePath);
            log.info("to: {}", destinationFilePath);
            File destinationFile = new File(destinationFilePath);
            if (!destinationFile.exists())
                makeDir(destinationFile);
            Files.copy(Path.of(sourceFilePath), Path.of(destinationFilePath), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("e.getMessage(): {}", e.getMessage());
            throw new IllegalStateException(e.getMessage());
        }
    }

    private static void makeDir(File destinationFile) {
        boolean isSuccess = destinationFile.mkdirs();
        if (!isSuccess) {
            String errorMsg = "無法建立該資料夾" + destinationFile;
            log.error(errorMsg);
            throw new IllegalStateException(errorMsg);
        }
    }

    public static List<String> readFile(String path) {
        List<String> datas = new LinkedList<>();
        try (BufferedReader br = getBufferedReader(path)) {
            String line;
            while ((line = br.readLine()) != null)
                datas.add(line);
        } catch (IOException e) {
            log.error("e.getMessage(): {}", e.getMessage());
            throw new IllegalStateException(e.getMessage());
        }

        return datas;
    }

    private static BufferedReader getBufferedReader(String path) throws FileNotFoundException {
        return new BufferedReader(new FileReader(path));
    }

}
