package com.ed.tpi;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class OISProductionUploadTool {
    public static void process(String[] args) throws IOException {
        // 需設定
        var sourcePathPrefix = "/Users/chenhuangkai/Documents/";            // set 你的專案目錄位置開頭
        var destinationPrefix = "/Users/chenhuangkai/Desktop/test/dest/";   // set 目的地目錄位置開頭
        var pathOfShell = "/Users/chenhuangkai/Documents/OISProductionUploadTool/src/main/resources/shell/ProduceChangeFile.sh";
        // 不動
        var isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
        var fileName = "change.txt";
        var projectPath = sourcePathPrefix + "OISAPI" + (isWindows ? "\\" : "/");
        var pathOfChangeFile = projectPath + fileName;
        var name = new SimpleDateFormat("yyyyMMdd_").format(new Date());
        var destinationRootFolderName = name + "ois_prod_OISAPI_v" + new Date().getTime();
        // 需設定
        var branchName = "正式機版本_v35_造字";  // set
        var commitAVersionNumber = "正式機版本_v34_轉檔email為null問題";  // set
        var commitBVersionNumber = "正式機版本_v35_造字";  // set

//
//        // 3. 異動清單
//        log.info("2. 建立異動清單統計");
//        var addFileContent = String.join("\n", addPaths);
//        var modifiedFileContent = String.join("\n", modifiedPaths);
//        var deleteFileContent = String.join("\n", deletePaths);
//
//        var content = "======異動清單統計========"
//                + "\n異動總數量: " + total
//                + "\n新增數量: " + addFile
//                + "\n修改數量: " + modifiedFile
//                + "\n刪除數量: " + deleteFile
//                + "\n--------------------------------------------------------"
//                + "\n新增檔案列表: "
//                + "\n" + addFileContent
//                + "\n--------------------------------------------------------"
//                + "\n修改檔案列表: "
//                + "\n" + modifiedFileContent
//                + "\n--------------------------------------------------------"
//                + "\n刪除檔案列表: "
//                + "\n" + deleteFileContent
//                + "\n--------------------------------------------------------";
//        var file = new File(destinationPrefix + destinationRootFolderName + "/change.txt");
//        try (FileOutputStream fos = new FileOutputStream(file)) {
//            fos.write(content.getBytes(Charset.defaultCharset()));
//        } catch (IOException e) {
//            log.error("e.toString(): {}", e.toString());
//            throw e;
//        }
//
//
//        // 4. copy ois.jar
//        log.info("4. copy ois.jar");
//        var jarPath = "OISAPI/target/ois.jar";
//        if (!new File(sourcePathPrefix + jarPath).exists())
//            throw new FileNotFoundException("File not exist" + sourcePathPrefix + jarPath);
//        copyFile(sourcePathPrefix + jarPath, destinationPrefix + destinationRootFolderName + "/" + "ois.jar");
//    }
    }

}






