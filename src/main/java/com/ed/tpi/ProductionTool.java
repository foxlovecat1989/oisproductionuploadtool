package com.ed.tpi;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import static com.ed.tpi.AppConstant.*;
import static com.ed.tpi.SystemInfoTool.getProjectPath;

@Slf4j
public class ProductionTool implements ActionListener {
    private static final String SHELL_FILENAME = "ProduceChangeFile.sh";
    private static final String PRODUCE_FILENAME = "change.txt";

    private JButton submitButton;
    private JButton cleanLogButton;
    private JTextArea displayArea;
    private JTextField projectNameInput;
    private JTextField shellDirInput;
    private JTextField sourcePathPrefixInput;
    private JTextField destinationInput;
    private JTextField branchInput;
    private JTextField commitAInput;
    private JTextField commitBInput;
    private boolean isWindows;

    public ProductionTool() {
        initUi();
        initEnvironment();
    }

    private void initUi() {
        JFrame frame = new JFrame("OIS Production Tool");
        frame.setSize(600, 750);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        frame.add(panel);
        placeComponents(panel);
        frame.setVisible(true);
    }

    private void initEnvironment() {
        isWindows = SystemInfoTool.isWindows();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ProductionTool::new);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        // input: 專案名稱
        JLabel projectNameLabel = new JLabel("專案名稱:");
        projectNameLabel.setBounds(10, 20, 120, 25);
        panel.add(projectNameLabel);
        projectNameInput = new JTextField(20);
        projectNameInput.setText(AppConstant.PROJECT_NAME);
        projectNameInput.setBounds(130, 20, 460, 25);
        panel.add(projectNameInput);

        // input: 專案目錄位置開頭
        JLabel sourcePathPrefixLabel = new JLabel("專案目錄位置開頭:");
        sourcePathPrefixLabel.setBounds(10, 50, 120, 25);
        panel.add(sourcePathPrefixLabel);
        sourcePathPrefixInput = new JTextField(20);
        sourcePathPrefixInput.setText(SOURCE_PATH_PREFIX);
        sourcePathPrefixInput.setBounds(130, 50, 460, 25);
        panel.add(sourcePathPrefixInput);

        // input: 目的地目錄位置
        JLabel destinationLabel = new JLabel("目的地目錄位置:");
        destinationLabel.setBounds(10, 80, 120, 25);
        panel.add(destinationLabel);
        destinationInput = new JTextField(20);
        destinationInput.setText(DEST_PATH);
        destinationInput.setBounds(130, 80, 460, 25);
        panel.add(destinationInput);

        // input: 存放shell目錄位置
        JLabel shellDirLabel = new JLabel("shell目錄位置:");
        shellDirLabel.setBounds(10, 110, 120, 25);
        panel.add(shellDirLabel);
        shellDirInput = new JTextField(20);
        shellDirInput.setText(SHELL_PATH);
        shellDirInput.setBounds(130, 110, 460, 25);
        panel.add(shellDirInput);

        // input: Branch
        JLabel branchLabel = new JLabel("Branch:");
        branchLabel.setBounds(10, 140, 120, 25);
        panel.add(branchLabel);
        branchInput = new JTextField(20);
        branchInput.setText(BRANCH_NAME);
        branchInput.setBounds(130, 140, 460, 25);
        panel.add(branchInput);

        // input: CommitA
        JLabel commitALabel = new JLabel("CommitA:");
        commitALabel.setBounds(10, 170, 120, 25);
        panel.add(commitALabel);
        commitAInput = new JTextField(20);
        commitAInput.setText("HEAD^");
        commitAInput.setBounds(130, 170, 460, 25);
        panel.add(commitAInput);

        // input: CommitB
        JLabel commitBLabel = new JLabel("CommitB:");
        commitBLabel.setBounds(10, 200, 120, 25);
        panel.add(commitBLabel);
        commitBInput = new JTextField(20);
        commitBInput.setText("HEAD");
        commitBInput.setBounds(130, 200, 460, 25);
        panel.add(commitBInput);

        // displayArea & JScrollPane
        displayArea = new JTextArea();
        JScrollPane jScrollPane = new JScrollPane(displayArea);
        jScrollPane.setBounds(10, 260, 580, 450);
        panel.add(jScrollPane);

        // input: PROCESS
        submitButton = new JButton("PROCESS");
        submitButton.setBounds(400, 225, 100, 30);
        submitButton.addActionListener(this);
        panel.add(submitButton);

        // cleanLogButton
        cleanLogButton = new JButton("Clean Log");
        cleanLogButton.setBounds(490, 225, 100, 30);
        cleanLogButton.addActionListener(this);
        panel.add(cleanLogButton);
    }

    public void actionPerformed(ActionEvent event) {
        try {
            if (event.getSource() == submitButton)
                processSubmit();
            if (event.getSource() == cleanLogButton)
                processCleanLog();
        } catch (Exception e) {
            logMsg("ERROR: ", e.getMessage());
            log.error(e.getMessage());
        }
    }

    private void processCleanLog() {
        displayArea.setText("");
    }

    private void processSubmit() {
        String branchName = branchInput.getText();
        String commitAInputText = commitAInput.getText();
        String commitBInputText = commitBInput.getText();
        String osName = System.getProperty("os.name").toLowerCase();
        String sourcePathPrefixInputAfterReplaceSlash = replaceCorrectSlash(sourcePathPrefixInput.getText());
        sourcePathPrefixInput.setText(sourcePathPrefixInputAfterReplaceSlash);
        String destinationInputAfterReplaceSlash = replaceCorrectSlash(destinationInput.getText());
        destinationInput.setText(destinationInputAfterReplaceSlash);
        String shellDirInputAfterReplaceSlash = replaceCorrectSlash(shellDirInput.getText());
        shellDirInput.setText(shellDirInputAfterReplaceSlash);

        logMsg("作業系統為: " + osName);
        logMsg("專案名稱: " + projectNameInput.getText());
        logMsg("專案目錄位置開頭: " + sourcePathPrefixInputAfterReplaceSlash);
        logMsg("目的地目錄位置: " + destinationInputAfterReplaceSlash);
        logMsg("change.txt path: " + getProjectPath() + PRODUCE_FILENAME);
        logMsg("Project Full Path: " + getProjectPath());
        logMsg("Shell位置: " + shellDirInputAfterReplaceSlash);
        logMsg("Branch: " + branchName);
        logMsg("CommitA: " + commitAInputText);
        logMsg("CommitB: " + commitBInputText);

        List<String> args = List.of(branchName, commitAInputText, commitBInputText, PRODUCE_FILENAME, getProjectPath());
        String shellFullPath = shellDirInput.getText() + SHELL_FILENAME;
        produceChangeFile(args, shellFullPath);

    }

    private String replaceCorrectSlash(String source) {
        String slash = isWindows ? AppConstant.LINUX_BACKWARD_SLASH : WINDOWS_FORWARD_SLASH;
        String replaceSlash = isWindows ? WINDOWS_FORWARD_SLASH : LINUX_BACKWARD_SLASH;
        while (source.contains(slash))
            source = source.replace(slash, replaceSlash);

        return source;
    }

    private void checkInput(String projectPath) {
        boolean isProjectPathExist = Paths.get(projectPath).toFile().isDirectory();
        if (!isProjectPathExist)
            throw new IllegalStateException(String.format("專案目錄不存在or不是目錄: %s", projectPath));
        boolean isDestinationPathExist = Paths.get(destinationInput.getText()).toFile().isDirectory();
        if (!isDestinationPathExist)
            throw new IllegalStateException(String.format("目的目錄不存在or不是目錄: %s", projectPath));
        boolean isShellExist = Paths.get(shellDirInput.getText() + SHELL_FILENAME).toFile().isFile();
        if (!isShellExist)
            throw new IllegalStateException(String.format("%s 不存在此目錄下: %s", SHELL_FILENAME, shellDirInput));
//        boolean isBranchNameChange = !branchInput.getText().equals(DEFAULT_BRANCH_NAME);
//        if (!isBranchNameChange)
//            throw new IllegalStateException("請輸入分支名稱");
    }

    private void logMsg(String... msgs) {
        String msg = String.join(" ", msgs);
        displayArea.append(msg + "\n");
    }

    private void produceChangeFile(List<String> args, String shellFullPath) {
        List<String> shellMessages = new ShellTool().executeShell(shellFullPath, args);
        shellMessages.forEach(this::logMsg);
        File file = new File(getProjectPath() + PRODUCE_FILENAME);
        boolean isSuccessToProduceNewFile =
                file.exists() && Math.abs(file.lastModified() - new Date().getTime()) < 10000;
        String msg =
                String.format(
                        "建立檔案結果: %s - %s",
                        isSuccessToProduceNewFile ? "Success" : "Fail",
                        file.getAbsolutePath());
        log.info(msg);
        logMsg(msg);
    }
}
