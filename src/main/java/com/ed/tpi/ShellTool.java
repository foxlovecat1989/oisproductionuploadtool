package com.ed.tpi;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Slf4j
public class ShellTool {

    public List<String> executeShell(String shellFullPath, List<String> args) {
        List<String> messages = new ArrayList<>();
        String msg = String.format("執行 Shell(%s)...", shellFullPath);
        log.info(msg);
        messages.add(msg);
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(getShellCmds(shellFullPath, args));
        try {
            Process process = processBuilder.start();
            int exitCode = process.waitFor();
            String successMsg = String.format("SUCCESS - Run <%s>", shellFullPath);
            String failMsg = String.format("Exited with error code : %s", exitCode);
            boolean isSuccess = exitCode == 0;
            String message = isSuccess ? successMsg : failMsg;
            messages = getShellMessages(process);
            messages.add(message);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error("Interrupted in executeShell()- e.getMessage(): {}", e.getMessage());
            throw new IllegalStateException(e.getMessage());
        } catch (IOException e) {
            log.error("IOException Error in executeShell()- e.getMessage(): {}", e.getMessage());
            throw new IllegalStateException(e.getMessage());
        }

        return messages;
    }

    private static List<String> getShellMessages(Process process) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        List<String> messages = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            log.debug(line);
            messages.add(line);
        }

        return messages;
    }

    private List<String> getShellCmds(String shellFullPath, List<String> args) {
        List<String> cmds = new LinkedList<>();
        String cmd = SystemInfoTool.isWindows() ? AppConstant.WINDOWS_CMD : AppConstant.LINUX_SH;
        cmds.add(cmd);
        cmds.add(shellFullPath);
        cmds.addAll(args);

        return cmds;
    }
}
