package com.ed.tpi;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.*;

import static com.ed.tpi.GitStatusEnum.*;

@Slf4j
public class StatisticalGitDiffTool {
    public static final List<String> ignoreKeyWords = List.of(".gitignore");

    public static List<String> getDiffFilePaths(String fileFullPath) {
        Map<GitStatusEnum, List<String>> statisticalDatas = statisticGitDiff(fileFullPath);
        List<String> addPaths = statisticalDatas.get(ADD);
        List<String> modifiedPaths = statisticalDatas.get(MODIFIED);
        List<String> deletePaths = statisticalDatas.get(DELETE);
        List<String> errorPaths = statisticalDatas.get(ERROR);
        List<String> pendingPaths = new ArrayList<>();
        pendingPaths.addAll(addPaths);
        pendingPaths.addAll(modifiedPaths);
        pendingPaths.addAll(deletePaths);
        pendingPaths.addAll(errorPaths);

        return pendingPaths;
    }

    private static Map<GitStatusEnum, List<String>> statisticGitDiff(String fileFullPath){
        List<String> addPaths = new LinkedList<>();
        List<String> modifiedPaths = new LinkedList<>();
        List<String> deletePaths = new LinkedList<>();
        List<String> errorPaths = new LinkedList<>();

        try (BufferedReader reader =
                     new BufferedReader(
                             new InputStreamReader(
                                     new FileInputStream(fileFullPath)))) {
            String oneLine;
            while ((oneLine = reader.readLine()) != null) {
                boolean isIgnorePath = isIgnorePath(oneLine);
                if (isIgnorePath)
                    continue;

                GitStatusEnum gitStatusEnum = GitStatusEnum.getEnum(String.valueOf(oneLine.charAt(0)));
                switch (gitStatusEnum) {
                    case MODIFIED:
                        modifiedPaths.add(replacePath(oneLine));
                        break;
                    case ADD:
                        addPaths.add(replacePath(oneLine));
                        break;
                    case DELETE:
                        deletePaths.add(replacePath(oneLine));
                        break;
                    case ERROR:
                    default:
                        errorPaths.add(replacePath(oneLine));
                }
            }

            Map<GitStatusEnum, List<String>> datas = new HashMap<>();
            datas.put(ADD, addPaths);
            datas.put(MODIFIED, modifiedPaths);
            datas.put(DELETE, deletePaths);
            datas.put(ERROR, errorPaths);

            return datas;
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new IllegalStateException(e.getMessage());
        }
    }

    private static String replacePath(String oneLine) {
        return !oneLine.contains("src") ?
                "OISAPI/" + oneLine.substring(oneLine.lastIndexOf("\t") + 1) :
                "OISAPI/" + oneLine.substring(oneLine.indexOf("src"));
    }

    private static boolean isIgnorePath(String oneLine) {
        return ignoreKeyWords.stream()
                .anyMatch(oneLine::equalsIgnoreCase);
    }
}

