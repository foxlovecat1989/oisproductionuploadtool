package com.ed.tpi;

import java.nio.file.Paths;

import static com.ed.tpi.AppConstant.LINUX_BACKWARD_SLASH;
import static com.ed.tpi.AppConstant.WINDOWS_FORWARD_SLASH;

public class SystemInfoTool {
    public static final String WINDOWS = "windows";
    public static final String OS_NAME = "os.name";

    public static boolean isWindows(){
        return getOsName().toLowerCase().startsWith(WINDOWS);
    }

    public static String getOsName(){
        return System.getProperty(OS_NAME);
    }

    public static String getProjectPath() {
        checkIsPathExist(AppConstant.PROJECT_PATH);

        return AppConstant.PROJECT_PATH;
    }

    private static void checkIsPathExist(String path) {
        boolean isProjectPathExist = Paths.get(path).toFile().isDirectory();
        if (!isProjectPathExist)
            throw new IllegalStateException(String.format("目錄不存在or不是目錄: %s", path));
    }
}
